#!/bin/sh

# simplistic detection of OS
if [ -f /etc/redhat-release ]; then
	PKG_MGR=yum
fi
if [ -f /etc/debian_version ]; then
	PKG_MGR=apt-get
fi

echo "OS uses the $PKG_MGR package manager"

# install git
sudo $PKG_MGR install -y git 

# lets clone  our .vimrc and move it around some
git clone -q https://github.com/dword4/vim-settings.git
cp vim-settings/.vimrc ~
mkdir -p .vim/colors && cp vim-settings/.vim/colors/*.vim .vim/colors
rm -rf vim-settings/
mkdir -p .vim/bundle && cd .vim/bundle

# now its time to pull down all the handy plugins
git clone -q https://github.com/scrooloose/nerdtree.git
git clone -q https://github.com/majutsushi/tagbar.git
git clone -q https://github.com/bling/vim-airline.git
git clone -q https://github.com/tpope/vim-surround.git
git clone -q https://github.com/gmarik/Vundle.vim $PWD/.vim/bundle/vundle # fix for Linux on Win10 Subsystem

# also we might want to install ctags while we are at it
sudo $PKG_MGR install -y ctags
# probably should also install vim and vim-enhanced along the way
sudo $PKG_MGR install -y vim

# time to add the alias
echo "alias vi='vim'" >> .bash_profile
