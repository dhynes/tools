#!/bin/sh
#
# probity.sh
# a simple file integrity check 

# configuration variables below

LOG_DIRECTORY='llogs/'

# do not modify any variables below this point
# unless you know what you are doing, it will
# probably break something

# before we get carried away, some cleanup to make
# things nice looking

clear
echo "Probity 0.1"
printf \\n

today=`date +%Y-%m-%d`
yesterday=`date --date="yesterday" +%Y-%m-%d`

t_log="$LOG_DIRECTORY""probity_$today"
y_log="$LOG_DIRECTORY""probity_$yesterday"

# lets check to make sure some files are present

if [ ! -f $t_log ];
then
	echo "the file $t_log appears to be missing"
	exit 0
fi

if [ ! -f $y_log ];
then
	echo "the file $y_log appears to be missing"
	exit 0
fi
results=`diff -q $t_log $y_log`

if [ -z "$results" ]
then
	echo "FILES MATCH"
else
	echo "FILES DIFFER"
	echo "running diff now"
	echo `diff --normal $t_log $y_log`
fi

