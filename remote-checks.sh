#!/bin/sh
#
# this script requires a ssh trust to be setup for the root account on THIS server with its id_rsa.pub key placed in /root/.ssh/authorized_keys on each
server it will be used to poll

HOST=""
HOST2=""

# function: checksvc
# argument: service name to check
# returns: string statement

checksvc(){
	SVC="$1"
	SV="service $SVC status"
	V=`ssh $HOST2 "$SV"`

	if echo $V | grep -q 'is running'
	then
	        echo "$1 is running"
	else
	        echo "$1 is NOT running"
	fi
	}
		
