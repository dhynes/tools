#!/bin/bash
source /etc/profile

# Device to check
devname="/dev/$1"

let p=`df -k $devname | grep -v ^File | awk '{printf ("%i",$3*100 / $2); }'`
if [ $p -ge 2 ]
then
  df -h $devname  
fi
