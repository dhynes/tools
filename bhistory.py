#!/usr/bin/python

"""bhistory.py: reads timestamps from .bash_history files"""

__author__	= "Drew Hynes"
__copyright__	= "yeah, right, the sensitive type!"

import re
import time
import sys
import os.path

try: sys.argv[1]

except: sys.exit("\nNo file specified!\nUsage:\nbhistory <filename>\n")

if os.path.isfile(sys.argv[1]):
	# file exists!
	print ""
else:
	sys.exit("\nfile doesn't exist, try again\n")

with open(sys.argv[1]) as input_file:
	for i, line in enumerate(input_file):
		#print line,
		m_obj = re.search("^\#",line)
		if m_obj:
			new = line.strip('#')
			print time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(int(new)))
		else :
			print line
print "{0}line(s) printed".format(i+1)
